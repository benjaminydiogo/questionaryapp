import { useState } from 'react';
import './App.css';
import SignIn from './Components/SignIn/SignIn';
import Form from './Components/Form/Form';
import {
  BrowserRouter as Router,
  Route, Routes

} from "react-router-dom";
import Login from './Components/Login';
import QuestionNum from './Components/QuestionNum'
import QuestionBool from './Components/QuestionBool';

function App() {
  const [keyAccepted, setKeyAccepted] = useState(false);
  const [key, setKey] = useState();

  const handleKey = (childKey) => {
    setKeyAccepted(true);
    setKey(childKey);
  }

  if (keyAccepted) {
    return (
      <Form other={key} />
    )
  }

  return (
   /*<SignIn sendKey={handleKey} />*/
    <>
    <Router>
        <Routes>
          <Route exact path="/" element={<Login/>} />
          <Route exact path="/signIn" element={<SignIn/>} />
          <Route exact path="/bol" element={<QuestionBool/>} />
          <Route exact path="/num" element={<QuestionNum/>} />



        </Routes>
      </Router>
    </>

  )



}


export default App;
