import { Slider } from "@mui/material";
import { useState } from 'react';
import Box from '@mui/material/Box';
import { Card, CardContent, FormLabel, Typography, Radio, FormControl, RadioGroup } from '@mui/material';
import Button from '@mui/material/Button';
import { Navigate, useNavigate } from "react-router-dom";


export default function CustomCard(props) {
    const navigate = useNavigate();
    const urls = ['/bol', '/num']
    let temp = 0;
    const handleSubmit = (e) => {
        e.preventDefault();
        console.log(222)
        if (temp === 0) {
            temp = 1;
        }
        else {
            temp = 0;
        }
        navigate(urls[temp])

    };
    return (
        <Card>

            <CardContent sx={{ textAlign: "center" }}>

                <Box
                    sx={{
                        marginTop: 8,
                        display: 'flex',
                        flexDirection: 'column',
                        alignItems: 'center',
                    }}
                >

                    <Box component="form" noValidate sx={{ mt: 1, width: "800px" }}>
                        {props.content}
                
                    </Box>
                </Box>
            </CardContent>
        </Card>
    )
}