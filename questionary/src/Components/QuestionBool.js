import { Card, CardContent, FormLabel, Typography, Radio, FormControl, RadioGroup } from '@mui/material';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Checkbox from '@mui/material/Checkbox';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import FormControlLabel from '@mui/material/FormControlLabel';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import axios from 'axios';
import * as React from 'react';
import { useState, useEffect } from 'react';

const theme = createTheme();

const Form = (props) => {

  const [question, setQuestion] = useState('');
  const [ansBool, setAnsBool] = useState(false);



  const handleAnsBool = (e) => {
    e.preventDefault();
    setAnsBool(e.target.value)


  };
  const handleSubmit = (e) => {
    e.preventDefault();
    console.log(222)
    if (temp === 0) {
        temp = 1;
    }
    else {
        temp = 0;
    }
    navigate(urls[temp])

};
  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xl">
        <CssBaseline />
        <Card>

          <CardContent sx={{ textAlign: "center" }}>

            <Box
              sx={{
                marginTop: 8,
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'center',
              }}
            >

              <Box component="form" noValidate sx={{ mt: 1, width: "800px" }}>
                <FormControl>
                  <FormLabel id="demo-controlled-radio-buttons-group">question</FormLabel>
                  <RadioGroup
                    aria-labelledby="demo-controlled-radio-buttons-group"
                    name="controlled-radio-buttons-group"
                    value={ansBool}
                    onChange={(e) => handleAnsBool(e)}>
                    <FormControlLabel value="true" control={<Radio />} label="True" />
                    <FormControlLabel value="false" control={<Radio />} label="False" />
                  </RadioGroup>
                </FormControl>

                <Button
                  type="submit"
                  fullWidth
                  variant="contained"
                  sx={{ mt: 3, mb: 2 }}
                >
                  submit
                </Button>
              </Box>
            </Box>
          </CardContent>
        </Card>
      </Container>
    </ThemeProvider>
  )
}

export default Form;