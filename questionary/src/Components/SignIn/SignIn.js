import HelpOutlineRounded from '@mui/icons-material/HelpOutlineRounded';
import Avatar from '@mui/material/Avatar';
import Box from '@mui/material/Box';
import Button from '@mui/material/Button';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { useState } from 'react';
import axios from 'axios';

const theme = createTheme();

const SignIn = (props) => {
  const [key, setKey] = useState('');



  const handleSubmit = () => {
    props.sendKey(key);

    /*axios.get('https://localhost:7056/key', key).then(response => {
      if (response.data) {
        props.sendKey(key);
      }
      else {
        alert(`The key ${key} doens't exist!`);
      }
    });*/
  };

  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <CssBaseline />
        <Box
          sx={{
            marginTop: 8,
            display: 'flex',
            flexDirection: 'column',
            alignItems: 'center',
          }}
        >
          <Avatar sx={{ m: 1, bgcolor: 'secondary.main' }}>
            <HelpOutlineRounded />
          </Avatar>
          <Typography component="h1" variant="h5">
            Crypto Form
          </Typography>
          <Box component="form" onSubmit={handleSubmit} noValidate sx={{ mt: 1 }}>
            <TextField
              margin="normal"
              required
              fullWidth
              id="key"
              label="Form Key"
              name="key"
              autoComplete="key"
              autoFocus
              onChange={(e) => setKey(e.target.value)}
            />
            <Button
              type="submit"
              fullWidth
              variant="contained"
              sx={{ mt: 3, mb: 2 }}
            >
              Go To Form
            </Button>
          </Box>
        </Box>
      </Container>
    </ThemeProvider>
  );
}

export default SignIn;