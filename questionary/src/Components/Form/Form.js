import { Card, CardContent, FormLabel, Typography, Radio, FormControl, RadioGroup } from '@mui/material';
import Box from '@mui/material/Box';
import Checkbox from '@mui/material/Checkbox';
import Container from '@mui/material/Container';
import CssBaseline from '@mui/material/CssBaseline';
import FormControlLabel from '@mui/material/FormControlLabel';
import { createTheme, ThemeProvider } from '@mui/material/styles';
import TextField from '@mui/material/TextField';
import axios from 'axios';
import * as React from 'react';
import { useState, useEffect } from 'react';

const theme = createTheme();

const Form = () => {


  return (
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xl">
        <CssBaseline />
       
      </Container>
    </ThemeProvider>
  )
}

export default Form;