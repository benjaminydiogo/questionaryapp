import { Slider } from "@mui/material";
import { useState } from 'react';
import Box from '@mui/material/Box';
import { Card, CardContent, FormLabel, Typography, Radio, FormControl, RadioGroup } from '@mui/material';
import CustomCard from "./CustomCard";


function QuestionsContent() {


    const [question, setQuestion] = useState();
    const [ansNum, setAnsNum] = useState(5);

    const handleAnsNum = (e) => {
        e.preventDefault();
        setAnsNum(e.target.value)
    }
    return (
        <>
            <Box component="form" noValidate sx={{ mt: 1, width: "800px" }}>
                <FormControl>
                    <FormLabel id="demo-controlled-radio-buttons-group">question</FormLabel>
                    <Slider min={1} max={10} step={1} aria-label="Default" valueLabelDisplay="auto" value={ansNum} onChange={(e) => handleAnsNum(e)} />
                </FormControl>
            </Box>
        </>

    )
}
export default function QuestionNum() {
    return <CustomCard content={<QuestionsContent />} />;
}
